<html>
<head>
<title>My Test Events</title>
<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-theme.min.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/app.css');?>">

<script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
</head>
<body>
    <div id="container" class="center-block">
        <h1 class="text-center">My Test Events</h1>
        
        <?=$this->search->createFilter($this->pagination->current_page(), $currentOrder, $currentSort)?>
        <div class="table-responsive col-lg-7 pull-left">
            <table class="table table-bordered table-hover">
            <thead>
                <tr><th>#</th><th>Event</th><th>Packages Count</th><th>Package</th><th>Fee</th><th>Start Date</th><th>End Date</th></tr>
            </thead>
            <tbody>
            <?php
            if($events){
                foreach ($events as $itEvent)
                {
                echo '<tr><td>'.$itEvent->id.'</td><td>'.$itEvent->event_name.'</td><td>'.$itEvent->packages_count.'</td><td>'.$itEvent->package_name.'</td><td>$ '.$itEvent->fee.' '.$itEvent->currency.'</td><td>'.$itEvent->event_begin_date.'</td><td>'.$itEvent->event_end_date.'</td></tr>';
                }
            }
            ?>
            </tbody>
            </table>
        </div>
	
	<?=$this->pagination->create_links()?>
    </div>
</body>
</html>