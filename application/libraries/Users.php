<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Users {
	var $idUsuario;
	var $nombre;
	var $apellido;
	var $i_pais_id;
	var $token;
	var $ci;
		
	function Users()
    {
		$this->ci =& get_instance();
		$this->ci->load->library('encrypt');
    }
	
	/**
	 * function _validatePass( $username, $password )
	 * Checks if a password matches the user.
	 */
	function _validatePass( $username, $password )
	{

		$query = $this->ci->db->get_where('poblacion', array('mail' => $username));

		if( $query->num_rows() > 1 )
		{
			foreach ($query->result() as $row)
			{
				$decrypted_password = $row->password;
				$this->idUsuario =  $row->iduf;
				$this->nombre =  $row->nombre;
				$this->apellido =  $row->apellido;
				$this->unidadfuncional =  $row->unidadfuncional;
			}

			if( $password === $decrypted_password )
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
		
	}
	
	/**
	 * function isLoggedIn()
	 * Checks if the current user is
	 * logged in
	 */
	function isLoggedIn()
	{
		if( $this->ci->session->userdata('usuario')!= NULL )
		{
			$userdata = unserialize($this->ci->encrypt->decode($this->ci->session->userdata('usuario')));
			
			if( $this->_validatePass($userdata['username'],$userdata['password']) )
			{
				$this->ci->user = $userdata['username'];
				return true;
			}
			else
			{
				//Cear the session for good measure
				$this->ci->session->sess_destroy();
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function isLoggedIn_inicio()
	{
		if( $this->ci->session->userdata('usuario')!= NULL )
		{
			$userdata = unserialize($this->ci->encrypt->decode($this->ci->session->userdata('usuario')));

			if( $this->_validatePass($userdata['username'],$userdata['password']) )
			{
				$this->ci->user = $userdata['username'];
				return true;
			}
			else
			{
				//Cear the session for good measure
				$this->ci->session->sess_destroy();
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	/**
	 * function login( $username, $password )
	 * Logs a user in if their information
	 * is correct.
	 */
	function login( $username, $password )
	{

		if( $this->_validatePass($username,$password) )
		{
		
			$userdata = $this->ci->encrypt->encode(serialize(array('username'=>$username,'password'=>$password, 'idusuario'=>$this->idUsuario, 'unidadfuncional'=>$this->unidadfuncional, 'nombre'=>$this->nombre, 'apellido'=>$this->apellido)));
			//Set the session data

			$this->ci->session->set_userdata('usuario',$userdata);
			
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function UsuarioLogueado(){
		$datos_usuario = unserialize($this->ci->encrypt->decode($this->ci->session->userdata('usuario')));
		return $datos_usuario;
	}
	
	function LoginCheck($token_pagina){
		$datos_usuario = unserialize($this->ci->encrypt->decode($this->ci->session->userdata('usuario')));
		$ok = 0;
		if($datos_usuario['token']){
			foreach ($datos_usuario['token'] as $token)
			{
				if($token == $token_pagina){
					$ok = 1;
					break;
				}
			}
		}
		if($ok==1){
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * function register( $username, $password, $email, $emailcheck = true )
	 * Registers a user if the given username is not in use.
	 * Optionally, you may chose to allow more than one account
	 * per email address by setting the fourth value to false.
	 * If there is more than one account per email, the recover
	 * password function will not work.
	 */
	function register( $username, $password, $email, $emailcheck = TRUE )
	{
		$query = $this->ci->db->getwhere('tbl_users', array('username' => $username));
		if( $query->num_rows() == 1 )
		{
			$this->last_error = "Username already in use.";
			return false;
		}
		else
		{	
			if( $emailcheck )
			{
				$query = $this->ci->db->getwhere('users', array('email' => $email));
				if( $query->num_rows() == 1 )
				{
					$this->last_error = "Email already in use.";
					return false;
				}
			}
			
			$encoded_password = $this->ci->encrypt->encode($password);
			
			$data = array(
					'username'  => $username,
					'password'  => $encoded_password,
					'email'		=> $email
					);
					
			$this->ci->db->insert('users',$data);
			
			return true;
		}
	}
	
	/**
	 * function logout()
	 * Logs out the current user.
	 */
	function logout()
	{
		$this->ci->db->delete('tbl_sessions', array('session_id' => $this->ci->session->userdata('session_id'))); 
		$this->ci->session->sess_destroy();
		return true;
	}
}
?>