<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Search {
    function getEventsPerPage()
    {
        return "10";
    }
    
    function CreatePaginationConfig($events_model, $orderBy, $sortType)
    {
        if(!isset($orderBy) OR !is_numeric($orderBy)){
            $orderBy = 0;
        }

        if(!isset($sortType) OR !is_numeric($sortType)){
            $sortType = 1;
        }
        //Pagination settings
        $config["per_page"]   = $this->getEventsPerPage();
        $config['use_page_numbers'] = FALSE;
        $config["base_url"]   = site_url("home");
        $config["uri_segment"] = "4";
        $config["num_links"] = "4";
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['prev_link'] = '&lt;';
        $config['next_link'] = '&gt;';
        $config["total_rows"] = $events_model->GetTotalEvents();
        $config['prefix'] = "/".$orderBy."/".$sortType."/";
        
        $config['full_tag_open'] = '<div id="pagination" class="col-xs-1"><ul class="list-group list-inline">';
        $config['full_tag_close'] = '</ul></div>';

        $config['first_tag_open'] = '<li class="list-group-item prev page">';
        $config['first_tag_close'] = '</li>';

        $config['last_tag_open'] = '<li class="list-group-item next page">';
        $config['last_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li class="list-group-item next page">';
        $config['next_tag_close'] = '</li>';

        $config['prev_tag_open'] = '<li class="list-group-item prev page">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="list-group-item active">';
        $config['cur_tag_close'] = '</li>';

        $config['num_tag_open'] = '<li class="list-group-item page">';
        $config['num_tag_close'] = '</li>';

        return $config;
    }

    function GetEvents($events_model, $num, $orderBy, $sortType)
    {
        //Parameters validation
        if(!isset($num) OR !is_numeric($num)){
            $num = -1;
        }

        if(!isset($orderBy) OR !is_numeric($orderBy)){
            $orderBy = 0;
        }

        if(!isset($sortType) OR !is_numeric($sortType)){
            $sortType = 1;
        }

        //Getting Events from model
        return $events_model->Events($this->getEventsPerPage(),$num,$orderBy,$sortType);
    }

    function createFilter($currentPage, $currentOrder, $currentSort)
    {
        if(!isset($currentOrder)){
            $currentOrder = 0;
        }

        if(!isset($currentSort)){
            $currentSort = 1;
        }
        
        $filterUrl = site_url("home")."/";
        $page = $currentPage ? "/".$currentPage : "";
        echo "<div id='filters' class='col-xs-4'>";
        echo "<div id='orderBy' class='pull-left'><h5>Order By: </h5>";
        echo "<ul class='list-group list-inline'>";
        switch ($currentOrder)
        {
            case 0:
            default:
                echo "<li class='list-group-item id active'>Event #</li>";
                echo "<li class='list-group-item price'><a href='".$filterUrl."1/".$currentSort.$page."'>Price</a></li>";
                echo "<li class='list-group-item date'><a href='".$filterUrl."2/".$currentSort.$page."'>Date</a></li>";
                break;
            case 1:
                echo "<li class='list-group-item id'><a href='".$filterUrl."0/".$currentSort.$page."'>Event #</a></li>";
                echo "<li class='list-group-item price active'>Price</li>";
                echo "<li class='list-group-item date'><a href='".$filterUrl."2/".$currentSort.$page."'>Date</a></li>";
                break;
            case 2:
                echo "<li class='list-group-item id'><a href='".$filterUrl."0/".$currentSort.$page."'>Event #</a></li>";
                echo "<li class='list-group-item date'><a href='".$filterUrl."2/".$currentSort.$page."'>Price</a></li>";
                echo "<li class='list-group-item date active'>Date</li>";
                break;
        }
        echo "</ul></div>";
        
        echo "<div id='sortType' class='pull-left'><h5>Sort Type: </h5>";
        echo "<ul class='list-group list-inline'>";
        switch ($currentSort)
        {
            case 0:
            default:
                echo "<li class='list-group-item asc active'>Descendant</li>";
                echo "<li class='list-group-item desc'><a href='".$filterUrl.$currentOrder."/1/".$page."'>Ascendant</a></li>";
                break;
            case 1:
                echo "<li class='list-group-item asc'><a href='".$filterUrl.$currentOrder."/0/".$page."'>Descendant</a></li>";
                echo "<li class='list-group-item desc active'>Ascendant</li>";
                break;
        }
        echo "</ul></div>";
        echo "</div>";
    }
}