<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class funciones {

	function funciones()
    {
		$this->ci =& get_instance();
	}

	public function getstrPathImagenes($strTexto, $i_articulo_id){
		$strTexto = str_replace("|%|PATHIMAGEN|%|", base_url().'articulos/images/'.$i_articulo_id.'/',$strTexto);
		return $strTexto;
	}

	public function edad($edad){
		list($anio,$mes,$dia) = explode("-",$edad);
		$anio_dif = 0;
		if ($anio>0){
			$anio_dif = date("Y") - $anio;
			$mes_dif = date("m") - $mes;
			$dia_dif = date("d") - $dia;
			if ($dia_dif < 0 || $mes_dif < 0)
			$anio_dif--;
		}
		return $anio_dif;
	}
	
	public function elimina_acentos($s){
		$letras_malas = array("�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�");
		$letras_buenas = array("a", "e", "i", "o", "u", "n", "A", "E", "I", "O", "U", "N");
		
		$s = str_replace($letras_malas, $letras_buenas, $s);  
		
		return $s;
	}
	
	public function dias_esp($dia){
		$dias = array("Domingo", "Lunes", "Martes", "Mi�rcoles", "Jueves",  "Viernes", "Sabado");
		return $dias[$dia];
	}
	
	public function meses_esp($mes){
		$meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo",  "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
		return $meses[$mes];
	}
	
	function CodigoHTML($Texto,$Acc) {
		if ($Acc == "C") {
			$Texto = ereg_replace('strong|STRONG','b',$Texto);
			$Texto = str_replace("'",'"',$Texto);
			$Texto = str_replace('"','#*#',$Texto);
			$Texto = str_replace('\n','###',$Texto);
			$Texto = str_replace('\\','',$Texto);
		  } else { 
			$Texto = str_replace('#*#','"',$Texto); 
			$Texto = str_replace('*#*',"'",$Texto);
			$Texto = str_replace('###',"\n",$Texto);
			$Texto = str_replace('\\','',$Texto);
		}
		return ($Texto);
	}
	
	function convert_smart_quotes($string) 
	{ 
		$search = array(chr(145), 
						chr(146), 
						chr(147), 
						chr(148), 
						chr(151)); 
	
		$replace = array("'", 
						 "'", 
						 '"', 
						 '"', 
						 '-'); 
	
		return str_replace($search, $replace, $string); 
	}
	
	function EmailTemplate($content, $turismo=false){
		$HTMLEmail = '<html xmlns="http://www.w3.org/1999/xhtml">
				<head>
				<title>WMHC2013.com</title>
			<style type="text/css">
			  <!--
				.Titulo {
				font-family: Georgia, "Times New Roman", Times, serif;
				font-size: 16px;
				color: #346c91;
				}
				.Textos {
					font-family: Georgia, "Times New Roman", Times, serif;
					color: #414042;
					font-size: 12px;
				}
			  -->
			</style>
				</head>
				<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
				<table width="90%" border="0" align="center" cellspacing="4">
					  <tr>
						<td colspan="2">&nbsp;</td>
					  </tr>
					  <tr>
						<td colspan="2" align="center" class="Titulo">Mensajes desde el sitio WMHC2013.com</td>
					  </tr>
					  <tr>
						<td colspan="2"><hr></td>
					  </tr>
					  <tr>
						<td width="150" align="right" class="Textos"><b>Nombre y Apellido:</b></td>
						<td width="350" class="Textos">'.$content['Nombre'].'</td>
					  </tr>
					  <tr>
						<td width="150" align="right" class="Textos"><b>e-Mail:</b></td>
						<td width="350" class="Textos">'.$content['Email'].'</td>
					  </tr>
					  <tr>
						<td width="150" align="right" class="Textos"><b>Celular:</b></td>
						<td width="350" class="Textos">'.$content['Telefono'].'</td>
					  </tr>';

					  if(!$turismo)
					  {
						  $HTMLEmail .= '<tr>
							<td width="150" align="right" class="Textos"><b>Profesi&oacute;n:</b></td>
							<td width="350" class="Textos">'.$content['Profesion'].'</td>
						  </tr>';
					  }

					  $HTMLEmail .= '<tr>
						<td width="150" align="right" class="Textos"><b>Pa&iacute;s:</b></td>
						<td width="350" class="Textos">'.$content['Pais'].'</td>
					  </tr>';

					  if(!$turismo)
					  {
						  $HTMLEmail .= '<tr>
							<td width="150" align="right" class="Textos"><b>Provincia:</b></td>
							<td width="350" class="Textos">'.$content['Provincia'].'</td>
						  </tr>
						   <tr>
							<td width="150" align="right" class="Textos"><b>Ciudad:</b></td>
							<td width="350" class="Textos">'.$content['Ciudad'].'</td>
						  </tr>';
					  }
					  else
					  {
						   $HTMLEmail .= '<tr>
								<td width="150" align="right" class="Textos"><bMotivo del mensaje:</b></td>
								<td width="350" class="Textos">'.$content['Motivo'].'</td>
							  </tr>';
					  }

					  $HTMLEmail .= '<tr>
						<td colspan="2"><hr></td>
					  </tr>
					  <tr>
						<td colspan="2" class="Textos">'.$this->CodigoHTML($content['Comentario'],'').'</td>
					  </tr>
					  <tr>
						<td colspan="2">&nbsp;</td>
					  </tr>
					</table>
				</body>
			</html>';

		return $HTMLEmail;
		
	}

}
?>
