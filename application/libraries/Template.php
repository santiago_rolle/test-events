<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Template {
		var $template_data = array();

		function set($name, $value)
		{
			$this->template_data[$name] = $value;
		}
	
		function load($template = '', $view = '' , $view_data = array(), $return = FALSE)
		{   
			$this->CI =& get_instance();
			
			$view_data['usuario'] = $this->CheckUsuario();						
			$this->set('contents', $this->CI->load->view($view, $view_data, TRUE));			
			return $this->CI->load->view($template, $this->template_data, $return);
		}
		
		function load_contenido($template = '', $view = '', $return = FALSE)
		{   
			$this->CI =& get_instance();

			$idarchivo = @fopen('./application/views/pages/'.$view, "r");
			
			$contenido = "";
			if ($idarchivo) {
				while (!feof($idarchivo)) {
					$buffer = fgets($idarchivo, 4096);
					$contenido = $contenido . $buffer;
					$contenido = str_replace("base_url()",base_url(),$contenido);
				}
			}
			else {
				$contenido = "El archivo no se pudo abrir para lectura";
			}
			@fclose($idarchivo);
			
			$this->set('usuario', $this->CheckUsuario());
			$this->set('contents', $contenido);
		
			return $this->CI->load->view('templates/'.$template, $this->template_data, $return);
		}
		
		function CheckUsuario(){
			$datos_usuario = null;
			if($this->CI->users->isLoggedIn()){
				$datos_usuario = $this->CI->users->UsuarioLogueado();
			}
			return $datos_usuario;
		}
}

/* End of file Template.php */
/* Location: ./system/application/libraries/Template.php */