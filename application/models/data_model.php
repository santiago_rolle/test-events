<?php

class Data_model extends CI_Model  {

	 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	function Events($num=-1,$offset=-1, $orderBy=OrderType::Id,$sortType=SortType::Asc){
		$this->db->select('events.id, events.event_name, event_packages.id as package_id, event_packages.package_name, event_packages.fee, event_packages.currency');
                $this->db->select("DATE_FORMAT(events.event_begin_date, '%m/%d/%Y') AS event_begin_date, DATE_FORMAT(events.event_end_date, '%m/%d/%Y') AS event_end_date, ", false);
                $this->db->select('(SELECT COUNT(p2.id) FROM event_packages p2 WHERE p2.event_id=events.id) as packages_count');
		$this->db->from('events');
		$this->db->join('event_packages', 'event_packages.event_id = events.id', 'INNER');
                $sort = $sortType==SortType::Asc? "asc" : "desc";
                switch ($orderBy) {
                    case OrderType::Price:
                        $this->db->order_by("event_packages.fee", $sort);
                        break;
                    case OrderType::Date:
                        $this->db->order_by("event_begin_date", $sort);
                        $this->db->order_by("event_packages.fee", "asc");
                        break;
                    case OrderType::Id:
                    default:
                        $this->db->order_by("id", $sort);
                        $this->db->order_by("event_packages.fee", "asc");
                        break;
                }
                
		if($num != -1 && $offset != -1){
                    $this->db->limit($num, ($offset));
		}
		else
		{
                    if($num != -1){
                        $this->db->limit($num);
                    }
		}
		$query = $this->db->get();
		
		return $query->result();
	}
        
        function GetTotalEvents(){
		return $this->db->count_all_results('events');
	}
}

abstract class OrderType
{
    const Id = 0;
    const Price = 1;
    const Date = 2;
}

abstract class SortType
{
    const Desc = 0;
    const Asc = 1;
}
?>