<?php
				
class Home extends CI_Controller 
{           
	public function __construct()
	{
            parent::__construct();
	}
	
	function _remap($function){
		switch ($function){
			case 'index':
				$this->home();
				break;
			default:
				$this->home();
				break;
		}
	}	
	
	function home(){	
                //Loading libraries
                $this->load->library('pagination');
                $this->load->library('search');
                
                //Getting parameters
                /* Query string is enabled. We can use 'orderBy' and 'sort'
                 * query string values to make URLs semantically friendlier. 
                $orderBy = $this->input->get('orderBy', true);
                $sort = $this->input->get('sort', true);
                $num = $this->uri->segment(2);
                 */
		$orderBy = $this->uri->segment(2);
                $sort = $this->uri->segment(3);
                $num = $this->uri->segment(4);
                
                //Creating settings for Pagination
                $config = $this->search->CreatePaginationConfig($this->Data_model, $orderBy, $sort);        
                
                //Initializing Pagination
		$this->pagination->initialize($config);
                
                //Preparing view model
                $data['events'] = $this->search->GetEvents($this->Data_model, $num, $orderBy, $sort);
                $data['currentOrder'] = $orderBy;
                $data['currentSort'] = $sort;
                
                //Returning data to View.
		$this->load->view('homeView', $data);
	}
}	
?>
